# Como executar o projeto

Consumo de Aplicação externa

### Tecnologias Utilizadas
* [Java 8]
* [Spring Boot 2]

### Ambiente necessário para execução
* [JDK 8]
* [Maven 3]

### Gerar pacote para publicação
1. [cd cadastro]
2. [mvn package]
3. [cd ./target]
4. [java -jar cadastro-0.0.1-SNAPSHOT.war]

### URL de acesso a aplicação
* Acessar a aplicação: http://localhost:8999/

### Documentação via Swagger
* http://localhost:8999/swagger-ui.html#/cadastro-controller
