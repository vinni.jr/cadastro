package br.com.api.cadastro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NegocioException extends RuntimeException{

	
	private static final long serialVersionUID = 8489228579825522791L;
	
	public NegocioException() {
		super();
	}
	
	public NegocioException(String mensagem) {
		super(mensagem);
	}

}
