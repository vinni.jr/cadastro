package br.com.api.cadastro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AplicacaoException extends RuntimeException {

	private static final long serialVersionUID = 459545305069352501L;
	
	public AplicacaoException() {
		super();
	}
	
	public AplicacaoException(String mensagem) {
		super(mensagem);
	}

}
