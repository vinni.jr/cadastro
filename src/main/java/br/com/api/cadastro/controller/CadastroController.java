package br.com.api.cadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.cadastro.model.Cliente;
import br.com.api.cadastro.service.CadastroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "Cadastro")
@RequestMapping("/cadastro")
public class CadastroController {
	
	@Autowired
	private CadastroService cadastroService;
	
	@ApiOperation(value = "Cadastrar cliente")
	@PostMapping(value = "/{cep}")
	public Cliente atualizarProcesso(@PathVariable String cep, @RequestBody Cliente cliente){
		return cadastroService.cadastrarCliente(cliente, cep);
	}
	
	@ApiOperation(value = "Retorna um cliente")
	@GetMapping(value = "/{email}")
	public Cliente obterCliente(@PathVariable String email){
		return cadastroService.obterCliente(email);
	}

}
