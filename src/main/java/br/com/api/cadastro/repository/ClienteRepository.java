package br.com.api.cadastro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.cadastro.model.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	Cliente findByEmail(String email);

}
