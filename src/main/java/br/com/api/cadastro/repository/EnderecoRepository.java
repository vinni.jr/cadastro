package br.com.api.cadastro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.cadastro.model.Endereco;


public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {
	
	Endereco findByCep(String cep);

}
