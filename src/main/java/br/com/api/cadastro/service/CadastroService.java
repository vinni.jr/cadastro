package br.com.api.cadastro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.cadastro.model.Cliente;
import br.com.api.cadastro.model.Endereco;

@Service
public class CadastroService {
	
	@Autowired
	private EnderecoService enderecoService;
	
	@Autowired
	private ClienteService clienteService;
	
	public Cliente cadastrarCliente(Cliente cliente, String cep) {
		Endereco endereco = enderecoService.obterEndereco(cep);
		Cliente clienteSalvo = cliente;
		clienteSalvo.setEndereco(endereco);
		return clienteService.salvarCliente(clienteSalvo);
	}
	
	public Cliente obterCliente(String email) {
		return clienteService.obterCliente(email);
	}

}
