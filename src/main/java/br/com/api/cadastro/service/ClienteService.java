package br.com.api.cadastro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.cadastro.exception.NegocioException;
import br.com.api.cadastro.model.Cliente;
import br.com.api.cadastro.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente salvarCliente(Cliente cliente) {
		try {
			return clienteRepository.save(cliente);
		}catch(Exception e) {
			throw new NegocioException("Erro ao cadastrar o cliente");
		}
	}
	
	public Cliente obterCliente(String email) {
		return clienteRepository.findByEmail(email);
	}
	
	
}
