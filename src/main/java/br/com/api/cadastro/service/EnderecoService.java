package br.com.api.cadastro.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import br.com.api.cadastro.exception.AplicacaoException;
import br.com.api.cadastro.exception.NegocioException;
import br.com.api.cadastro.model.Endereco;
import br.com.api.cadastro.repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	private WebClient webClient;

	@Autowired
	private EnderecoRepository enderecoRepository;

	private static final Logger LOG = LogManager.getLogger(EnderecoService.class);

	public Endereco obterEndereco(String cep) {
		String cepValid = validarCep(cep);
		Endereco endereco = enderecoRepository.findByCep(cepValid);

		if (!validarEndereco(endereco)) {
			return endereco;
		} else {

			try {
				cep.replace("-", "");
				endereco = this.webClient.get().uri(uriBuilder -> uriBuilder.path("ws/" + cep + "/json/").build(cep))
						.retrieve().bodyToMono(Endereco.class).block();
			} catch (Exception e) {
				LOG.error("Erro ao obter endereco. " + e.getMessage());
				throw new AplicacaoException("Ocorreu um erro inesperado.");
			}
			if (!validarEndereco(endereco)) {
				LOG.error("O endereço nao pode ser vazio.");
				throw new NegocioException("O endereço nao pode ser vazio.");
			}
			return enderecoRepository.save(endereco);
		}
	}

	private boolean validarEndereco(Endereco endereco) {
		return endereco == null || endereco.getId() == null || endereco.getCep().isEmpty()
				|| endereco.getLogradouro().isEmpty();
	}
	
	private String validarCep(String cep) {
		if(cep.contains("-")) {
			return cep;
		}else {
			String cepnovo = cep.substring(0,5)+"-"+cep.substring(5);
			return cepnovo;
		}
		
	}

}
